# @summary Installs and manages the PIDService
#
# @param servername
#   The fqdn name of the server.
#
# @param enable_pidsvc_proxy
#   True if /pidsvc should be proxied from localhost:8080/pidsvc
#
# @param proxy_pass
#   An array for the proxy pass rules for the main vhost.
#
# @param proxy_pass_match
#   Equivalent to proxy_pass except accepts regular expressions.
#
# @param rewrite_rules
#   An array for the rewrite rules for the main vhost.
#
# @param headers
#   Adds lines to replace the header for the apache vhost configuration.
#
# @param purge_unmanaged_conf
#   If set to true, all unmanaged apache (by puppet) configs and 
#   vhosts are removed.
#
# @param enable_ssl
#   Enable ssl for both apache and tomcat.
#
# @param ssl_cert_pem_path
#   The full path to the SSL cert pem.
#
# @param ssl_cert_key_path
#   The full path to the SSL cert key.
#
# @param ssl_cert_chain_path
#   The full path to the SSL cert chain.
#
# @param db_user
#   The database user name
#
# @param db_passwd
#   The database password for the pidsvc-admin user.
#
# @param declare_db_user
#   True if this module should manage the database user and false otherwise.
#
# @param postgres_password
#   The password to the postgresql database
#
# @param postgres_version
#   The postgresql version to use.  If left undef, uses the default for
#   the postgresql puppet module.
#
# @param postgis_version
#   Enable postgis extension for the database with the specified version.
#
# @param use_default_vhost
#   Allow this module to control the vhost and false otherwise.
#   If false, proxy_pass and rewrite_rules are ignored.
#
# @param listen_addresses
#   The address range that is allowed for the postgresql db.
#
# @param set_hba_rules
#   True if this class handles setting the postgresql rules and false
#   if its handled outside of this class.
#
# @param ipv4_acls
#   Sets the acls for the postgresql server.
#
#  @example
#  class { pidservice: }
#
class pidservice (
  $servername,
  $enable_pidsvc_proxy  = $pidservice::params::enable_pidsvc_proxy,
  $proxy_pass           = $pidservice::params::proxy_pass,
  $proxy_pass_match     = $pidservice::params::proxy_pass_match,
  $rewrite_rules        = $pidservice::params::rewrite_rules,
  $headers              = $pidservice::params::headers,
  $purge_unmanaged_conf = $pidservice::params::purge_unmanaged_conf,
  $enable_ssl           = $pidservice::params::enable_ssl,
  $ssl_cert_pem_path    = $pidservice::params::ssl_cert_pem_path,
  $ssl_cert_key_path    = $pidservice::params::ssl_cert_key_path,
  $ssl_cert_chain_path  = $pidservice::params::ssl_cert_chain_path,
  $db_user              = $pidservice::params::db_user,
  $db_passwd            = $pidservice::params::db_passwd,
  $declare_db_user      = $pidservice::params::declare_db_user,
  $postgres_password    = $pidservice::params::postgres_password,
  $postgres_version     = $pidservice::params::postgres_version,
  $postgis_version      = $pidservice::params::postgis_version,
  $use_default_vhost    = $pidservice::params::use_default_vhost,
  $listen_addresses     = $pidservice::params::listen_addresses,
  $set_hba_rules        = $pidservice::params::set_hba_rules,
  $ipv4_acls            = $pidservice::params::ipv4_acls,
  $tomcat_version       = '7.0.85',
  $java_version         = '8',
  $jdbc_url             = $pidservice::params::jdbc_url,
) inherits pidservice::params{

  # == additional variables ==
  $resource_url      = "jdbc:postgresql://${servername}:5432/pidsvc"
  $resource_username = $db_user
  $resource_password = $db_passwd

  # The default location for the pidservice configuration.
  $root              = '/etc/pidservice'

  contain pidservice::install
  contain pidservice::db
  contain pidservice::config::apache
  contain pidservice::config::tomcat

  # the next set of classes can be executed in any order
  # note, tomcat and db cannot have a relationship due to both using
  # the dreaded puppetlabs/apt which causes horrible depedency cycles!
  Class['pidservice::install'] -> Class['pidservice::db']
  Class['pidservice::install'] -> Class['pidservice::config::apache']
  Class['pidservice::install'] -> Class['pidservice::config::tomcat']
}
