# @summary Manages the database configuration.
#
class pidservice::db {

  # check if the postgresql version is to be set.
  if $pidservice::postgres_version != undef  or
    $pidservice::postgis_version != undef {
    class { 'postgresql::globals':
      manage_package_repo => true,
      version             => $pidservice::postgres_version,
      postgis_version     => $pidservice::postgis_version,
      before              => Class['postgresql::server'],
    }
    contain postgresql::globals
  }

  class { 'postgresql::server':
    listen_addresses     => $pidservice::listen_addresses,
    postgres_password    => $pidservice::postgres_password,
    pg_hba_conf_defaults => false,
  }
  contain postgresql::server

  if $pidservice::postgis_version != undef {
    contain postgresql::server::postgis
  }

  if $pidservice::set_hba_rules {
    Postgresql::Server::Pg_hba_rule {
      database => 'all',
      user     => 'all',
    }

    postgresql::server::pg_hba_rule { 'local all':
      type        => 'local',
      user        => 'postgres',
      auth_method => 'peer',
      order       => '001',
    }

    postgresql::server::pg_hba_rule {
      'local is for Unix domain socket connections only':
      type        => 'local',
      user        => 'all',
      auth_method => 'peer',
      order       => '002',
    }

    postgresql::server::pg_hba_rule { 'ipv4 local connections':
      type        => 'host',
      user        => 'all',
      address     => '127.0.0.1/32',
      auth_method => 'md5',
      order       => '003',
    }

    postgresql::server::pg_hba_rule { 'landcare ipv4 connections':
      type        => 'host',
      user        => 'all',
      address     => '172.0.0.0/8',
      auth_method => 'md5',
      order       => '004',
    }

    postgresql::server::pg_hba_rule { 'allow access to ipv6 localhost':
      type        => 'host',
      address     => '::1/128',
      auth_method => 'md5',
      order       => '101',
    }
  }

  if $pidservice::declare_db_user {
    postgresql::server::role {$pidservice::db_user:
      password_hash =>
      postgresql_password($pidservice::db_user,$pidservice::db_passwd),
      before        => Postgresql::Server::Database['pidsvc']
    }
  }

  postgresql::server::database {'pidsvc':
    owner => $pidservice::db_user,
  }

  archive{'postgresql.sql':
    path           => "${pidservice::root}/postgresql.sql",
    source         => 'https://www.seegrid.csiro.au/subversion/PID/trunk/pidsvc/src/main/db/postgresql.sql',
    allow_insecure => true,
  }

  # create the language, need to do this via command since
  # its not in the postgresql module and not time in the project to
  # add it properly
  include check_run
  check_run::task{'create_language':
    exec_command => '/usr/bin/createlang plpgsql pidsvc',
    user         => 'postgres',
    require      => [Postgresql::Server::Database['pidsvc'], Archive['postgresql.sql']],
    returns      => ['0','1','2'],
  }

  #TODO appears the root/postgresql.sql is not running, debug this
  check_run::task{'run_sql':
    exec_command => "/usr/bin/psql -d pidsvc -f ${pidservice::root}/postgresql.sql",
    user         => 'postgres',
    require      => Check_run::Task['create_language'],
    returns      => ['0','1','2'],
  }
}
