# @summary manages the root directory.
#
class pidservice::install {
  # make sure that these directories exists
  file {$pidservice::root:
    ensure  => directory,
  }
}
