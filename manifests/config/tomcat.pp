# @summary manages tomcat
#
class pidservice::config::tomcat {

  class { 'tomcat':
    version             => $pidservice::tomcat_version,
    java_version        => $pidservice::java_version,
    enable_tomcat_proxy => $pidservice::enable_ssl,
    tomcat_proxy_name   => 'localhost',
    tomcat_proxy_port   => '443',
  }
  contain tomcat

  tomcat::resource{'pidsvc':
    context => template('pidservice/context.xml.erb'),
  }

  tomcat::app{'pidsvc':
    war_url => 'https://cgsrv1.arrc.csiro.au/swrepo/PidService/jenkins/trunk/pidsvc-latest.war',
  }

  tomcat::shared_lib{'jdbc':
    url     => $pidservice::jdbc_url,
    require => Tomcat::App['pidsvc'],
  }
}
