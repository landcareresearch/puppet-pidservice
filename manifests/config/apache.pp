# @summary Sets up apache configuration for pidservice.
#
class pidservice::config::apache {

  class { '::apache':
    # not sure why the server name is specified as it shouldn't be!!!
    #servername    => 'test.liberator.zen.landcareresearch.co.nz',
    default_vhost => false,
    purge_configs => $pidservice::purge_unmanaged_conf,
  }

  contain apache
  contain apache::mod::rewrite
  contain apache::mod::proxy
  contain apache::mod::headers
  contain apache::mod::proxy_http
  contain apache::mod::ssl

  if $pidservice::use_default_vhost == true {
    # setup the proxy which needs to be combined with user specified proxy.
    # used for setting up apache
    if $pidservice::enable_pidsvc_proxy == true {
      # check for ssl as that should be proxy passed instead of 8080
      if $pidservice::enable_ssl {
        $proxy_pass_array = [
          { 'path' => '/pidsvc',
            'url'  => 'http://localhost:8080/pidsvc',
          }
        ]
      }else{
        $proxy_pass_array = [
          { 'path' => '/pidsvc',
            'url'  => 'http://localhost:8080/pidsvc',
          }
        ]
      }

      # concat the arrays.
      if $pidservice::proxy_pass == undef {
        $proxy_pass_array_total = $proxy_pass_array
      }else {
        $proxy_pass_array_total =
        concat($pidservice::proxy_pass,$proxy_pass_array)
      }
    }else {
      $proxy_pass_array_total = $pidservice::proxy_pass
    }


    if $pidservice::enable_ssl{
      # non-ssl
      apache::vhost { "${pidservice::servername}_non-ssl":
        servername => $pidservice::servername,
        port       => '80',
        docroot    => '/var/www',
        rewrites   => [
          {
            comment      => 'redirect to https',
            rewrite_cond => ['%{REQUEST_URI} !=/server-status', '%{HTTPS} off'],
            rewrite_rule => ['(.*) https://%{HTTP_HOST}:443%{REQUEST_URI}'],
          },
        ],
      }
      apache::vhost { "${pidservice::servername}_ssl":
        servername           => $pidservice::servername,
        port                 => '443',
        docroot              => '/var/www',
        default_vhost        => true,
        ssl                  => true,
        ssl_proxyengine      => true,
        ssl_cert             => $pidservice::ssl_cert_pem_path,
        ssl_key              => $pidservice::ssl_cert_key_path,
        ssl_chain            => $pidservice::ssl_cert_chain_path,
        proxy_preserve_host  => true,
        proxy_pass           => $proxy_pass_array_total,
        proxy_pass_match     => $pidservice::proxy_pass_match,
        redirectmatch_status => ['permanent'],
        redirectmatch_regexp => ['^/$ /static/'],
        rewrites             => $pidservice::rewrite_rules,
        headers              => $pidservice::headers,
#        custom_fragment      => "  ## additional ssl proxy engine params
#  SSLProxyVerify none
#  SSLProxyCheckPeerCN off
#  SSLProxyCheckPeerName off
#  SSLProxyCheckPeerExpire off",
      }
    }else {
      apache::vhost { $pidservice::servername:
        port                 => '80',
        docroot              => '/var/www',
        proxy_preserve_host  => true,
        proxy_pass           => $proxy_pass_array_total,
        proxy_pass_match     => $pidservice::proxy_pass_match,
        redirectmatch_status => ['permanent'],
        redirectmatch_regexp => ['^/$ /static/'],
        rewrites             => $pidservice::rewrite_rules,
        headers              => $pidservice::headers,
        default_vhost        => true,
      }
    }
  }
}
