# @summary The default parameters for pidservice.
#
class pidservice::params {

  $enable_pidsvc_proxy  = true
  $proxy_pass           = undef
  $proxy_pass_match     = undef
  $rewrite_rules        = undef
  $headers              = undef
  $purge_unmanaged_conf = true
  $enable_ssl           = false
  $db_user              = 'pidsvc-admin'
  $db_passwd            = 'pass'
  $declare_db_user      = true
  $postgres_password    = undef
  $postgres_version     = undef
  $postgis_version      = undef
  $use_default_vhost    = true
  $listen_addresses     = '*'
  $set_hba_rules        = true
  $jdbc_url             = 'https://jdbc.postgresql.org/download/postgresql-9.4-1201.jdbc4.jar'
  $ssl_cert_pem_path   = '/etc/ssl/certs/ssl-cert-snakeoil.pem'
  $ssl_cert_key_path   = '/etc/ssl/private/ssl-cert-snakeoil.key'
  $ssl_cert_chain_path = undef
  $ipv4_acls = [
'local   all             postgres                                peer',
'local   all             all                                     peer',
'host    all             all             127.0.0.1/32            md5',
'host    all             all             172.0.0.0/8           md5',
'host    all             all             ::1/128                 md5'
]
  $resource_name                                    = 'jdbc/pidsvc'
  $resource_auth                                    = 'Container'
  $resource_type                                    = 'javax.sql.DataSource'
  $resource_driver_class_name                       = 'org.postgresql.Driver'
  $resource_max_active                              = '-1'
  $resource_min_idle                                = '0'
  $resource_max_idle                                = '10'
  $resource_max_wait                                = '10000'
  $resource_min_evictable_idle_time_millis          = '300000'
  $resource_time_between_eviction_runs_millis       = '300000'
  $resource_num_tests_per_eviction_run              = '20'
  $resource_pool_prepared_statements                = true
  $resource_max_open_prepared_statements            = '100'
  $resource_test_on_borrow                          = true
  $resource_access_to_underlying_connection_allowed = true
  $resource_validation_query                        = 'SELECT VERSION();'
}
