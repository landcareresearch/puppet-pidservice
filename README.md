# PID Service Puppet Module

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/pidservice.svg)](https://forge.puppetlabs.com/landcareresearch/pidservice)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetPidservice_PuppetPidservice%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetPidservice_PuppetPidservice&guest=1)

## About

Installs the [PIDService](https://www.seegrid.csiro.au/wiki/Siss/PIDService) and suporting software such as Tomcat & Postgresql.

## API

See REFERENCE.md

## Usage

```puppet
  class {'pidservice':
    servername => 'localhost',
  }
```

Open a web browser to http://<hostname>:8080/pidsvc

## Limitations

Only works with debian based OS's.
