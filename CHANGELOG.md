# PIDService Changelog

## 2021-03-10 Release 6.0.2

- Updated pdk for Puppet 6 compliance.

## 2020-05-01 Release 6.0.1

- Added a parameter for installing a specific version of postgresql jdbc file.

## 2020-04-16 Release 6.0.0

- Fixed readme and changelog with markdown linting.
- Removed anchor pattern and now using contains
- Updated for Puppet 6 compliance.
- Removed wget dependency and replaced with archive because wget is depricated.

## 2018-07-26 Release 0.3.2

- Moved comments to puppet strings
- Added nocert check to wget
- Changed wget puppet module

## 2018-03-27 Release 0.3.1

- The keystore parameters were not being used.  So references to the keystore have been removed.
- Expects apache to proxy for ssl to its port 8080 which is served on localhost.

## 2018-03-09 Release 0.3.0

- Increased the minor version number due to change in tomcat puppet module dependency.
- Fixed formatting in change log.

## 2018-03-09 Release 0.2.10

- Changed from using the tomcat7 puppet module to the tomcat module.
- Added supporting parameters.

## 2015-03-04 Release 0.1.4

- Updated dependancies

## 2015-03-04 Release 0.1.3

- Migrated from github to bitbucket.
- Changed ownership to landcareresearch on puppetforge.
- Added new parameter for setting the version of postgresql.
- Updated readme to include parameter and added additional banners.
- Changed formatting of changlog.
- Fixed a bug with leaving proxypass as undef.
- Fixed issue with create language.
- Now dowloading initial sql file from main pidsvc trunk
- Added an option to install postgis

## 2014-05-01 Release 0.1.2

- Cleaned up source code to comply with PuppetForge scores.

## 2014-04-18 Release 0.1.0

- Initial Release.
